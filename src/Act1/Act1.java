
package Act1;

public class Act1 {
    
    public static void main(String[] args){
        
        for(int i=1; i<=1000; i++){
            if(i%3==0 && i%5==0 && i%7==0){
                System.out.println("FizzBuzzChuzz");
            }
            else if(i%3==0 && i%5==0){
                System.out.println("FizzBuzz");
            }
            else if(i%3==0 && i%7==0){
                System.out.println("FizzChuzz");
            }
            else if(i%5==0 && i%7==0){
                System.out.println("BuzzChuzz");
            }
            else{
                System.out.println(i);
            }
        }
        
    }
    
}
