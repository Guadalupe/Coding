
package Act2;

import java.util.Scanner;

public class Act2 {
    
    public static void main(String[] args){
        
        String a;
        int edad=3000;
        
        Scanner f = new Scanner (System. in);
        
        System.out.println("Introdusca su año de nacimiento: ");
        a=f.next();
        
        while(num(a)==false || Integer.parseInt(a)<0 || Integer.parseInt(a)>2017){
            System.out.println("Introdusca un año correcto: ");
            a=f.next();
        }
        edad=2017-Integer.parseInt(a);
        if(edad>100)
            System.out.println("Este usuario ya falleció.");
        else
            System.out.println("Su edad es: "+edad+" años");
        
    }
    
    public static Boolean num(String a){
        try{
            Integer.parseInt(a);
            return true;
        }
        catch(NumberFormatException e){
            return false;
        }
    }
    
}
